import threading
from StockTwitAPI import getPosts, saveObject
import pprint as pp
from SlackProcessNotifier import notifyProcessRun



def processPost(stock):
    posts = getPosts(stock)
    pp.pprint(posts)
    saveObject(posts, stock)

def main():
    notifyProcessRun()
    stocks = ["DJIA","AMZN","GE","IBM", "F", "SQ","AXP","TSLA", "NKE" ,"GPRO", "JNJ" , "NTNX", "SBUX", "CVX","SNAP"]

    threads = [threading.Thread(target=processPost, args=(stock,)) for stock in stocks]

    for thread in threads:
        print (thread)
        thread.start()
    for thread in threads:
        thread.join()
main()
