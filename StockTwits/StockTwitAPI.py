import urllib3
import json
import pprint as pp
import time


from pymongo import MongoClient

ACCESS_TOKEN = "97a1992ae3cd49c4fde95a153758153a877efe85"
URL_HEADER = "https://api.stocktwits.com/api/2/"
def getTrendingSymbols():
  
    url = URL_HEADER+'trending/symbols.json?access_token='+ACCESS_TOKEN
    symbols = callUrlByGet(url)
    return symbols["symbols"]


def getTopNTrendingSymbols(n=5):
    symbols = getTrendingSymbols()
    return symbols[0:n]

def callUrlByGet(url):
    http = urllib3.PoolManager()
    response = http.request('GET', url)
    
    data = json.loads(response.data.decode("UTF-8"))
    return data

def saveObject(objs, symbol):
    client = MongoClient('10.152.0.3',27017)
    db = client.StockTwits
    collection = None

    if symbol.lower() == "f":
        collection = db.F
    elif symbol.lower() == "ibm":
        collection = db.IBM
    elif symbol.lower() == "amzn":
        collection = db.AMZN
    elif symbol.lower() == "djia":
        collection = db.DJIA
    elif symbol.lower() == "ge":
        collection = db.GE
    elif symbol.lower() == "sq":
        collection = db.SQ
    elif symbol.lower() == "axp":
        collection = db.AXP
    elif symbol.lower() == "tsla":
        collection = db.TSLA
    elif symbol.lower() == "nke":
        collection = db.NKE
    elif symbol.lower() == "gpro":
        collection = db.GPRO
    elif symbol.lower() == "jnj":
        collection = db.JNJ
    elif symbol.lower() == "ntnx":
        collection = db.NTNX
    elif symbol.lower() == "sbux":
        collection = db.SBUX
    elif symbol.lower() == "cvx":
        collection = db.CVX
    elif symbol.lower() == "snap":
        collection = db.SNAP

    for obj in objs:

        if collection.find({"id":obj["id"]}).count() ==0:
            collection.insert(obj)

def getMinMaxID(symbol):
    client = MongoClient('localhost',27017)
    db = client.StockTwits
    collection = None

    if symbol.lower() == "f":
        collection = db.F
    elif symbol.lower() == "ibm":
        collection = db.IBM
    elif symbol.lower() == "amzn":
        collection = db.AMZN
    elif symbol.lower() == "djia":
        collection = db.DJIA
    elif symbol.lower() == "ge":
        collection = db.GE

    minimum = 0
    maximum = 0

    try:
        minimum = collection.sort([("id", 1)])[0]["id"]
        maximum = collection.sort([("id", -1)])[0]["id"]
    except Exception:
        minimum = 0
        maximum = 0
    
    
    return minimum, maximum

def getPosts(symbol="DJIA", new=True):
    
    # symbols = getTopNTrendingSymbols()
   
    messages = []
    lastIDs =[]

    resume = False

    """
        May be needed in the future
    """
    # minimum, maximum = getMinMaxID(symbol)
    # if (minimum>0 and maximum>0):
        # if minimum not in lastIDs:
            # lastIDs.append(minimum)
            # resume = True

    for o in range(5):
        url = ""
        if (len(lastIDs)>0):
            if (resume==False):
                url = URL_HEADER+"streams/symbol/"+symbol+".json?access_token="+ACCESS_TOKEN+"&max="+str(lastIDs[o-1])
            else:
                url = URL_HEADER+"streams/symbol/"+symbol+".json?access_token="+ACCESS_TOKEN+"&max="+str(lastIDs[o])
        else:
            url = URL_HEADER+"streams/symbol/"+symbol+".json?access_token="+ACCESS_TOKEN

        data = callUrlByGet(url)
        lastIDs.append(data["messages"][-1]["id"])

        for i in data["messages"]:
        
            temp = dict()
            
            temp["message"] = i["body"]
            temp["id"] = i["id"]

            temp["created_at"] =  i["created_at"]
            if (i["entities"]["sentiment"] != None):
                temp["sentiment"] = i["entities"]["sentiment"]["basic"]
            else:
                temp["sentiment"] = None

            temp["is_reshare_message"] = False
            if ("reshare_message" in i):
             
                temp["is_reshare_message"] = True
                temp["reshared_message"] = dict()
                temp["reshared_message"]["message"] = i["reshare_message"]["message"]["body"]
                temp["reshared_message"]["id"] = i["reshare_message"]["message"]["id"]
                temp["reshared_message"]["created_at"] =i["reshare_message"]["message"]["created_at"]
                
                if (i["reshare_message"]["message"]["entities"]["sentiment"]!= None):
                    temp["reshared_message"]["sentiment"] = i["reshare_message"]["message"]["entities"]["sentiment"]["basic"]
                else:
                    temp["reshared_message"]["sentiment"] = None

            if temp["is_reshare_message"] == True and temp["reshared_message"]["sentiment"] == None:
                if temp["sentiment"] == None:
                    continue
            elif temp["is_reshare_message"] == False:
                if temp["sentiment"] == None:
                    continue

            messages.append(temp)
            
        time.sleep(20)


    return messages
        


# pp.pprint(getPosts(symbol="AMZN"))
