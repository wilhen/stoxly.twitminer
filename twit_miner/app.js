var app = require('express')();
var http = require('http').Server(app);
var Twitter = require('twit');
var bodyParser = require('body-parser');
var logger = require('morgan');
const Slack = require('slack-node');

var stream = null;

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const Tweet = require("./models/tweets");
// const StanfordCoreNLPClient = require('corenlp-client');

var slack = new Slack();
slack.setWebhook(process.env.SLACK_WEBHOOK_URI);


const client = new Twitter({
    consumer_key: process.env.TWITTER_CONSUMER_KEY,
    consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
    access_token: process.env.TWITTER_BEARER_TOKEN,
    access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
});



app.post('/stream', function(req, res){

    if (stream!=null){
        console.log("Stopping Stream...");
        stream.stop();
        console.log("Stream Stopped");
        console.log("Changing Filter...");
    }

    stream = client.stream('statuses/filter',{track: req.body.ticker, language:'en'});

    // stream.on('connect', function(request){
    //     console.log(request);
    // });

    stream.on('limit', function (limitMessage) {
        console.log("hitting the limit...");
        stream.stop();
        console.log(limitMessage);
        console.log("Send Slack Notification...");

        notifySlack("`Limit Hit!` \n```"+limitMessage+"```", function (response){
            if(response.statusCode==200){
                console.log("Notified!");
            }
        }, function(error){
            console.log("Slack Notification is unavailable!");
        });
        
    });

    stream.on('connected', function (response){
        console.log('Connected!');

        notifySlack("Connected!", function (response){
            if(response.statusCode==200){
                console.log("Notified!");
            }
        }, function(error){
            console.log("Slack Notification is unavailable!");
        });
        
        if (!res.headersSent){
            res.status(201).send({message:response});
        }
         
    });

    stream.on('message',function(message){

        console.log(message);

        analyseSentiment(message.text,function(success){
        
            
            // create a ticker storage
            // store the tweet and sentiment analysis result
            //create object model
            
            var tickers = {};
            tickers.status = new Set();
            tickers.quoted_status = new Set();
            tickers.retweeted_status = new Set();
            
            //main status
            if(!message.hasOwnProperty("quoted_status")
                && !message.hasOwnProperty("retweeted_status")){
                    var symbols = message.entities.symbols;
                    for(var ii=0;ii<symbols.length;ii++){
                        tickers.status.add((symbols[ii]).text.toUpperCase());
                    }

                    if(message.truncated){
                        var extendedTweetSymbols = message.extended_tweet.entities.symbols;
                        for(var ii=0;ii<extendedTweetSymbols.length;ii++){
                            tickers.status.add((extendedTweetSymbols[ii]).text.toUpperCase());
                        }
                    }
                }
            
            //check for quoted status
            if(message.hasOwnProperty("quoted_status")){
                var quotedSymbols = message.quoted_status.entities.symbols;
                for(var ii=0;ii<quotedSymbols.length;ii++){
                    tickers.quoted_status.add((quotedSymbols[ii]).text.toUpperCase());
                }
                if(message.quoted_status.truncated){
                    var extendedTweetSymbols = message.quoted_status.extended_tweet.entities.symbols;
                    for(var ii=0;ii<extendedTweetSymbols.length;ii++){
                        tickers.quoted_status.add((extendedTweetSymbols[ii]).text.toUpperCase());
                    }
                }
            }

            //check for retweeted status
            if(message.hasOwnProperty("retweeted_status")){
                var retweetedSymbols = message.retweeted_status.entities.symbols;
                for(var ii=0;ii<retweetedSymbols.length;ii++){
                    tickers.retweeted_status.add((retweetedSymbols[ii]).text.toUpperCase());
                }
                if(message.retweeted_status.truncated){
                    var extendedTweetSymbols = message.retweeted_status.extended_tweet.entities.symbols;
                    for(var ii=0;ii<extendedTweetSymbols.length;ii++){
                        tickers.retweeted_status.add((extendedTweetSymbols[ii]).text.toUpperCase());
                    }
                }
            }

            tickers.status = Array.from(tickers.status);
            tickers.quoted_status = Array.from(tickers.quoted_status);
            tickers.retweeted_status = Array.from(tickers.retweeted_status);

            var tweets  = new Tweet({
                status: message,
                id: message.id_str,
                tickers:tickers,
                sentiment: success
            });
            // save the new data
            tweets.save(function(err){
                if (err) {
                    notifySlack("`Error` \n```"+err+"```", function (response){
                        if(response.statusCode==200){
                            console.log("Notified!");
                        }
                    }, function(error){
                        console.log("Slack Notification is unavailable!");
                    });
                }else{
			console.log('Tweet saved successfully!');
		}
            });
            
        });
    });

    stream.on("error",function(err){
        console.log("Stream Error!");
        console.log(err);
        stream.stop();
        console.log("Sending error to Slack...");

        notifySlack("`Error` \n```"+err+"```", function (response){
            if(response.statusCode==200){
                console.log("Notified!");
            }
        }, function(error){
            console.log("Slack Notification is unavailable!");
        });

        if (!res.headersSent){
            res.status(500).send(err);
        }
            
        
    });
});

function analyseSentiment(text, success, error){

        // Detects the sentiment of the text
        
        // nlu.analyze({
        //     'text': text, // Buffer or String 
        //     'features': {
        //         'sentiment':{}
        //     }
        // }, function(err, response) {
        //     if (err){
        //         console.log("error");
        //         console.log(err);
        //         error(err)
        //     }else{
               
        //         console.log(JSON.stringify(response, null, 2));
                var response = {};
                success(response)
        //     }
                
        // });
}

function notifySlack(text, output, error){
    slack.webhook({
        channel: "@williamhenry",
        username: "Stoxly",
        text: text
    }, function(err, response) {
        if (err){
            error(err);
        }
        output(response);
    });

}

http.listen(3001, function(){
    console.log('listening on *:3001');
});
