const dotenv = require('dotenv').config();
const mongoose =  require('mongoose');
 
mongoose.connect(process.env.MONGO_URL);
var Schema = mongoose.Schema;

var tweetSchema = new Schema ({
    status : {type: Object, required:true},
    id: {type: String, required:true, unique:true},
    tickers: {type: Object, required:true},
    sentiment : {type: Object, required:true}
});

var TweetModel = mongoose.model('djia',tweetSchema);


module.exports = TweetModel;
