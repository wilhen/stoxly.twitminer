from newsapi import NewsApiClient
from pymongo import MongoClient
import json
from pprint import pprint
import hashlib
import time
from datetime import datetime

newsapi = NewsApiClient(api_key='194207e4ead14eada6e43e8ecafe6ead')


def connect():
    client = MongoClient('10.152.0.3',27017)
    db = client.News
    return db

def getLatestDate(event, runningDate = None):
    if runningDate !=None:
        return runningDate
    
    db = connect()
    collection = None

    if event.lower() == "merger":
        collection = db.merger
    elif event.lower() == "acquisition":
        collection = db.acquisition
    elif event.lower() == "dividend":
        collection = db.dividend
    elif event.lower() == "profit":
        collection = db.profit
    elif event.lower() == "earnings":
        collection = db.earnings
    elif event.lower() == "stock split":
        collection = db.stock_split

    if collection.find({}).count() == 0:
        return '2017-01-01'
    else:
        collections = collection.find({}).sort("publishedAt", -1)
        collections = list(collections)
        publishedAt = collections[0]["publishedAt"]
        date = datetime.strptime(publishedAt,'%Y-%m-%dT%H:%M:%SZ').strftime("%Y-%m-%d") #Fri Aug 04 08:12:40 +0000 2017
        return date

def saveObject(objs, event):
    db = connect()
    collection = None

    if event.lower() == "merger":
        collection = db.merger
    elif event.lower() == "acquisition":
        collection = db.acquisition
    elif event.lower() == "dividend":
        collection = db.dividend
    elif event.lower() == "profit":
        collection = db.profit
    elif event.lower() == "earnings":
        collection = db.earnings
    elif event.lower() == "stock split":
        collection = db.stock_split

    for obj in objs:

        if collection.find({"id":obj["id"]}).count() ==0:
            collection.insert(obj)

def getNews(query, p=1, runningDate=None):
    sources = "bloomberg,cnn,cnbc,business-insider,techcrunch"
   
    from_date = getLatestDate(query, runningDate)

    articles = newsapi.get_everything(q=query,
                                      sources=sources,
                                      from_parameter=from_date,
                                      language='en',
                                      sort_by='relevancy',
                                      page_size=100,
                                      page=p)
   
    totalResults = articles["totalResults"]
    articlesObject = articles["articles"]
    # pprint(all_articles)
    articleTemp = []
    for i in articlesObject:
        
        article = dict()
        # temp = dict()
        article["author"] = i["author"]
        article["source"] = i["source"]["id"]
        article["title"] = i["title"]
        article["publishedAt"] = i["publishedAt"]
        article["description"] = i["description"]
        article["url"] = i["url"]
        article["topic"]= query.replace(" ","_")

        # temp["author"] = i["author"]
        # temp["source"] = i["source"]["id"]
        # temp["title"] = i["title"]
        js = None
        if i["author"]==None:
            js = "author_"+i["source"]["id"]+"_"+ i["publishedAt"] #json string
        else:
            js = i["author"]+"_"+i["source"]["id"]+"_"+ i["publishedAt"] #json string

        m = hashlib.sha512(js.encode("utf-8"))
        article["id"] = m.hexdigest()
        articleTemp.append(article)
    
    if len(articleTemp)==0:
        # if no article found (page> totalPages || no updates found on the given time)
        # then stop the process
        return

    saveObject(articleTemp, query)
    totalPages = totalResults//100
    if totalResults%100 >0:
        # residual pages
        totalPages +=1

    if (p<totalPages):
        page = p+1
        # time.sleep(20)
        getNews(query, p= page, runningDate= from_date)

# getNews("merger")