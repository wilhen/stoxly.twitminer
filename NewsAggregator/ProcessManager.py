import threading
from NewsAggregator import getNews
def aggregateNews(query):
    getNews(query)

def main():
   
    queries = ["merger", "acquisition","dividend", "profit","stock split","earnings"]

    threads = [threading.Thread(target=aggregateNews, args=(q,)) for q in queries]

    for thread in threads:
        print (thread)
        thread.start()
    for thread in threads:
        print(thread)
        thread.join()
main()
